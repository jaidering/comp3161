module MinHS.Evaluator where
import qualified MinHS.Env as E
import MinHS.Syntax
import MinHS.Pretty
import qualified Text.PrettyPrint.ANSI.Leijen as PP

type VEnv = E.Env Value

data Value = I Integer
           | B Bool
           | Nil
           | Cons Integer Value
           | Close VEnv Exp --closure for letfun bindings         
           deriving (Show)

instance PP.Pretty Value where
  pretty (I i) = numeric $ i
  pretty (B b) = datacon $ show b
  pretty (Nil) = datacon "Nil"
  pretty (Cons x v) = PP.parens (datacon "Cons" PP.<+> numeric x PP.<+> PP.pretty v)
  pretty _ = undefined -- should not ever be used

evaluate :: Program -> Value
evaluate [Bind _ _ _ e] = evalE E.empty e
evaluate bs = evalE E.empty (Let bs (Var "main"))
evalE :: VEnv -> Exp -> Value

evalE env (Num n) = I n
evalE env (Con "True") = B True
evalE env (Con "False") = B False
evalE env (Con "Nil") = Nil

evalE env (App(Prim Neg)(Num n)) = I(-n)
evalE env (App(Prim Neg)e1) = evalE env (App(Prim Neg)(Num n)) where I n = evalE env e1
evalE env (App(App(Prim Add)(Num n))(Num m)) =  I (n+m)
evalE env (App(App(Prim Sub)(Num n))(Num m)) =  I (n-m)
evalE env (App(App(Prim Quot)(Num n))(Num m)) = I(quot n m)
evalE env (App(App(Prim Mul)(Num n))(Num m)) = I(n*m)
evalE env (App(App(Prim Eq)(Num n))(Num m)) = B(n==m)
evalE env (App(App(Prim Ne)(Num n))(Num m)) = B(n/=m)
evalE env (App(App(Prim Gt)(Num n))(Num m)) = B(n>m)
evalE env (App(App(Prim Ge)(Num n))(Num m)) = B(n>=m)
evalE env (App(App(Prim Lt)(Num n))(Num m)) = B(n<m)
evalE env (App(App(Prim Le)(Num n))(Num m)) = B(n<=m)

evalE env (App(Prim Null)(Con "Nil")) = B True
evalE env (App(Prim Null)(Var id)) = evalE env (App(Prim Null)v) where v = devalV ((evalE env(Var id)))
evalE env (App(Prim Null)e1) = B False
evalE env (App(App(Prim p)e1) (e2)) = evalE env (App(App(Prim p)v1) v2) where v1 = devalV(evalE env e1)
							        	      v2 = devalV(evalE env e2)

evalE env (If(Con "True") e1 e2) = evalE env e1
evalE env (If(Con "False") e1 e2) = evalE env e2
evalE env (If e1 e2 e3) = evalE env(If tf e2 e3) where tf = devalV (evalE env e1)

evalE env (App(App(Con "Cons")(Num n))(Con "Nil")) = Cons n Nil
evalE env (App(App(Con "Cons")(Num n))e2) = Cons n (evalE env e2)

evalE env (Prim p) = Close env (Prim p)
evalE env (App(Prim p)(e1)) = Close env (App(Prim p) v1) where v1 = devalV (evalE env e1)  
   
evalE env (Letfun(Bind fName typ [vars]fExp)) = Close env' (Letfun b') where b' = (Bind fName typ [vars] fExp)
									     env' = E.add (env)(fName, Close env(Letfun b'))

evalE env (Letfun(Bind fName typ (x:xs)fExp)) = Close env' (Letfun b') where b' = (Bind fName typ (x:xs) fExp)
									     env' = E.add (env) (fName, Close env(Letfun b'))
   
evalE env (Letfun(Bind fName typ []fExp)) = Close env' (Letfun b') where b' = (Bind fName typ [] fExp)
									 env' = E.add (env) (fName, Close env(Letfun b'))

evalE env (App(Var id) exp) =
  case evalE env (Var id) of 
      Close env' (Letfun(Bind fName typ [var]fBody)) -> evalE funcEnv fBody where arg = evalE env exp
		                            					  funcEnv = E.addAll (env')[(var, arg), (fName, Close env'(Letfun(Bind fName typ[var]fBody)))]

      Close env' (Letfun(Bind fName typ (x:xs)fBody)) -> Close envRed letfunRed where arg = evalE env exp
                                 						      letfunRed = (Letfun (Bind fName typ(xs)fBody))
                                  						      envRed = E.addAll(env')[(x, arg), (fName, Close env' letfunRed)]
      Close env' (Letfun(Bind fName typ []fBody)) -> evalE env' (App fBody exp)
    
evalE env (App(Letfun b)e1) =
  case b of   
              (Bind fName typ [vars]fExp) -> evalE env' fExp where val = evalE env e1
                                             			   env' = (E.addAll (env)[(vars, val), (fName, Close env(Letfun b))])

              (Bind fName typ (x:xs)fBody) -> Close envRed letfunRed  where arg = evalE env e1
                                 					    letfunRed = (Letfun (Bind fName typ(xs)fBody))
                                  					    envRed = E.addAll (env) [(x, arg), (fName, Close env letfunRed)]

              (Bind fName typ [] fExp) -> evalE env (App fExp e1)

evalE env (Let [b](e2)) = 
   case b of 
      Bind vName typ [] e1 -> evalE(E.add(env)(vName,(evalE env e1))) e2
      Bind vName typ vars e1 -> evalE env'(e2) where env' = E.add(env)(vName, Close env(Letfun b))

evalE env (Let (b:bs)expFinal) = evalE env'(Let (bs) expFinal) where Bind vName typ vars e1 = b
               							     env' = E.add(env)(vName, val)
             							     val = evalE env e1

evalE env (Var id) =
   case E.lookup env id of Just res -> res 
                           Nothing -> error("Error variable not in environment -->" ++(show id)++"<-- Existing envrionment is -->" ++(show env))

evalE g e = error("Unimplented, environment is -->" ++(show g)++ "<-- exp is -->" ++(show e)++"<--")

devalV(I n) = Num n
devalV(Nil) = Con "Nil"
devalV(B True) = Con "True"
devalV(B False) = Con "False" 
devalV (Cons n e1) = App (App(Con "Cons")(Num n))(devalV e1)
devalV e = error("devalV not implemented for -->"++(show e)++"<----")
