/* DO NOT EDIT: This file is automatically generated by Cabal */

/* package ansi-wl-pprint-0.6.7.1 */
#define VERSION_ansi_wl_pprint "0.6.7.1"
#define MIN_VERSION_ansi_wl_pprint(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  6 || \
  (major1) == 0 && (major2) == 6 && (minor) <= 7)

/* package base-4.5.0.0 */
#define VERSION_base "4.5.0.0"
#define MIN_VERSION_base(major1,major2,minor) (\
  (major1) <  4 || \
  (major1) == 4 && (major2) <  5 || \
  (major1) == 4 && (major2) == 5 && (minor) <= 0)

/* package containers-0.4.2.1 */
#define VERSION_containers "0.4.2.1"
#define MIN_VERSION_containers(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  4 || \
  (major1) == 0 && (major2) == 4 && (minor) <= 2)

/* package optparse-applicative-0.10.0 */
#define VERSION_optparse_applicative "0.10.0"
#define MIN_VERSION_optparse_applicative(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  10 || \
  (major1) == 0 && (major2) == 10 && (minor) <= 0)

/* package parsec-3.1.2 */
#define VERSION_parsec "3.1.2"
#define MIN_VERSION_parsec(major1,major2,minor) (\
  (major1) <  3 || \
  (major1) == 3 && (major2) <  1 || \
  (major1) == 3 && (major2) == 1 && (minor) <= 2)

